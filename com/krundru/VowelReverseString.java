package com.krundru;

import java.util.Arrays;

public class VowelReverseString {

  /*
   *  while until start < end.
   *
   *    while: from start, find next vowel until start < end  // start:0
   *    while: from end, find prev vowel until start < end. // end: 4
   *    if ( start < end) swap vowels.
   *    start++; end--
   */
  public static void main(String[] args) {
    char [] letters = new char[] {'f','r','e','i','d' };
    swapVowels(letters);
    System.out.println(Arrays.toString(letters) );
  }

  private static void swapVowels(char[] arr) {
    int start = 0;
    int end = arr.length - 1;
    while (start < end) {
      while (start < end) {
        if (isVowel(arr[start])) {
          break;
        }
        start++;
      }
      while (start < end) {
        if (isVowel(arr[end])) {
          break;
        }
        end--;
      }
      if (start < end) {
        char temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
      }

      start++;
      end--;
    }
  }

  private static boolean isVowel(char ch) {
    return ch == 'i' || ch == 'e' || ch == 'a'; // add all vowels.
  }
}