package com.krundru;

import java.util.Arrays;

public class MatrixRotationMain {

  public static void main(String[] args) {

    int mat [][] = new int [][] {
      {1,2,3,4,5},
      {6,7,8,9,10},
      {11,12,13,14,15},
      {16,17,18,19,20},
      {21,22,23,24,25}
    };
    for (int i = 0; i < mat.length; i++) {
      System.out.println(Arrays.toString(mat[i]));
    }

    rotateMatrixBy90(mat);
    System.out.println("\n" );
    for (int i = 0; i < mat.length; i++) {
      System.out.println(Arrays.toString(mat[i]));
    }
  }

  private static void rotateMatrixBy90(int arr[][]) {
    int maxLayers = arr.length / 2; // [0... maxLayers)
    for (int li = 0; li < maxLayers; li++) {
      int top = li;
      int right = arr.length - 1 - li;
      int bottom = arr.length - 1 - li;
      int left = li;

      for (int i = left; i < right; i++) {
        int offset = i - left;

        int temp = arr[top][left + offset];
        // top <- left
        arr[top][left + offset] = arr[bottom- offset][left];
        // left <- bottom
        arr[bottom- offset][left] = arr[bottom][right - offset];
        // bottom <- right
        arr[bottom][right - offset] = arr[top + offset][right];
        // right <- top
        arr[top + offset][right] = temp;
      }
    }
  }
}